function createCard(name, description, pictureUrl, startsDate, endsDate, location) {
    return `
        <div class="col">
            <div class="card shadow">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer text-muted">${startsDate} - ${endsDate}</div>
            </div>
        </div>
    `;
  }

function createAlert() {
    return `
    <div class="alert alert-danger" role="alert">
    The URL can not be accessed
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll('.col');

    try {
      const response = await fetch(url);

      if (!response.ok) {
        const bodyTag = document.querySelector('body');
        const error = createAlert('The URL can not be accessed');
        bodyTag.innerHTML += error;
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = details.conference.starts;
            const startsDate = new Date(starts).toLocaleDateString();
            const ends = details.conference.ends;
            const endsDate = new Date(ends).toLocaleDateString();
            const location = details.conference.location.name
            const html = createCard(name, description, pictureUrl, startsDate, endsDate, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;

          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error(e);
      alert(e);

    }

  });
